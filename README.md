# infrastructure-images-packer

Packer configuration to build the images used for Freedesktop SDK's
infrastructure deployed on our OpenStack tenancy provided by OSUOSL.

## Ansible

Configuration of each system is performed via the ansible roles within the
`ansible/` subdirectory.

### Structure

Roles and playbooks are generally split by which system or service they are
targetting, then split again based on whether the tasks are executed at build
time (`setup`) or at first boot (`config`).

## Development Workflow

Builds are handled via GitLab CI, though any build jobs must be manually
triggered via the GitLab UI once a merge request pipeline has been created.

The build is currently split into 3 stages:

* upload-os: This runs `scripts/upload-os`, which will takes a URL to a cloud
  image and its checksum. While this stage needs to be run each time, it will be
  almost instantaneous on subsequent runs since it will verify whether an image
  with the provided checksum already exists prior to importing an image,
  skipping any additional processing if so.

* build-base: This will build the base image layer, which will include common
  dependencies and configurations between services.

* build-final: This will build any images layered on top of the base image,
  currently this will only build the `gitlab-runner` image.

### Updating the OS Image

Update the OS image by appropriately updating any of the `.cloud_image_vars`
variables within the CI YAML. Variables are based on the URL parts of images
within https://cloud.debian.org/images/cloud/.

### Submission to Live Infrastructure

Triggering a test deployment to the live infrastructure is, for the time being,
a manual process, whether this be a dev build or production.

Once a build job has completed, you can extract the name and ID from the CI
logs, e.g.:

```
2024-03-07T19:10:25Z: ==> openstack.base: Waiting for image debian-fdsdk-base-67b6c492-2024-03-07T18:57:35Z (image id: 5a4be6be-cd3f-4a26-bf1f-193326f1efdb) to become ready...
```

and push a branch to infrastructure-live with relevant ID updated within
`variables.tf`.
