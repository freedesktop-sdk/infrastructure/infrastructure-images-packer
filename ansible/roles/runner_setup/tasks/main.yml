---
- name: Enable backports
  ansible.builtin.apt_repository:
    repo: deb http://deb.debian.org/debian {{ ansible_distribution_release }}-backports main
    state: present
    filename: backports
    update_cache: yes

- name: Install gpg
  ansible.builtin.apt:
    name:
      - gpg
      - gpg-agent
    install_recommends: no

- name: Install performance debugging tools
  ansible.builtin.apt:
    name:
      - iotop
      - iftop
    install_recommends: no

- name: Install zram-tools
  ansible.builtin.apt:
    name:
      - zram-tools
    install_recommends: no

- name: Deploy zram config
  ansible.builtin.copy:
    mode: '0644'
    content: |
      ALGO=zstd
      PERCENT=60
    dest: /etc/default/zramswap

- name: Restart zramswap
  ansible.builtin.service:
    name: zramswap
    state: reloaded

- name: Add gitlab-runner gpg key
  ansible.builtin.get_url:
    url: https://packages.gitlab.com/runner/gitlab-runner/gpgkey
    dest: /etc/apt/trusted.gpg.d/gitlab-runner.asc
    mode: '0644'

- name: Add gitlab-runner apt repository
  ansible.builtin.apt_repository:
    repo: "deb https://packages.gitlab.com/runner/gitlab-runner/debian/ bookworm main"
    update_cache: yes

- name: Install systemd-resolved nss plugin
  ansible.builtin.apt:
    name:
      - libnss-resolve
    install_recommends: no

- name: Install required packages
  ansible.builtin.apt:
    name:
      - binfmt-support
      - gitlab-runner
      - qemu-user-static
    default_release: "{{ ansible_distribution_release }}-backports"
    install_recommends: no

- name: Enable gitlab-runner
  ansible.builtin.systemd:
    name: gitlab-runner
    enabled: yes

- name: Make VM compaction less aggressive
  ansible.posix.sysctl:
    name: vm.compaction_proactiveness
    value: '0'
    state: present

- name: Use stub resolver
  ansible.builtin.file:
    src: /run/systemd/resolve/stub-resolv.conf
    dest: /etc/resolv.conf
    owner: root
    group: root
    state: link
