build {
  sources = [
    "source.openstack.base",
    "source.openstack.runner"
  ]

  provisioner "ansible" {
    playbook_file = "ansible/${source.name}_setup.yml"
    use_proxy     = false
    sftp_command  = "/usr/lib/ssh/sftp-server -e"
    ansible_env_vars = [
      "ANSIBLE_SSH_ARGS=-C -o ControlMaster=auto -o ControlPersist=60s -o ServerAliveInterval=20"
    ]
    extra_arguments = [
      "-v",
      "-e ansible_python_interpreter=/usr/bin/python3"
    ]
  }

  post-processor "manifest" {
    output = "${source.name}_manifest.json"
  }
}
