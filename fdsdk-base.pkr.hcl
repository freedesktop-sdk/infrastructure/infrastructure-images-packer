variable "os_checksum" {
  default = env("CLOUD_IMAGE_CHECKSUM")

  validation {
    condition     = length(var.os_checksum) > 0
    error_message = <<EOF
Invalid variable "os_checksum".
EOF
  }
}

source "openstack" "base" {
  cloud      = "openstack"
  flavor     = "m1.medium"
  image_name = "debian-fdsdk-base-${var.short_sha}-${timestamp()}"
  source_image_filter {
    filters {
      properties = {
        os_hash_value = var.os_checksum
      }
    }
  }
  ssh_username = "debian"
  networks     = ["e52f5435-0e93-4749-9317-ee350046a31d"]
  image_tags = [
    "fdsdk-base-${var.branch}"
  ]
}
