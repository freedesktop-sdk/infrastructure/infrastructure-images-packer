source "openstack" "runner" {
  cloud      = "openstack"
  flavor     = "m1.medium"
  image_name = "debian-fdsdk-runner-${var.short_sha}-${timestamp()}"
  source_image_filter {
    filters {
      tags = ["fdsdk-base-${var.branch}"]
    }
    most_recent = true
  }
  ssh_username = "debian"
  networks     = ["e52f5435-0e93-4749-9317-ee350046a31d"]
}
