packer {
  required_plugins {
    ansible = {
      version = ">= 1.1.0"
      source  = "github.com/hashicorp/ansible"
    }
    openstack = {
      source  = "github.com/hashicorp/openstack"
      version = ">= 1.0.1"
    }
  }
}
