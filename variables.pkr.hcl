variable "short_sha" {
  default = env("CI_COMMIT_SHORT_SHA")

  validation {
    condition     = length(var.short_sha) > 0
    error_message = <<EOF
Invalid variable "short_sha".
EOF
  }
}

variable "branch" {
  default = env("CI_COMMIT_REF_SLUG")

  validation {
    condition     = length(var.branch) > 0
    error_message = <<EOF
Invalid variable "branch".
EOF
  }
}
